@component('admin.layouts.elements.body')
    @slot('title') Páginas @endslot
    @slot('description') Administração de Páginas @endslot

    <form class="form-horizontal" action="{{ route('pages.store')  }}" method="post">
        @include('admin.pages.form')
    </form>

    <a href="{{ route('pages.index') }}" class="btn btn-xs btn-default">voltar</a>
@endcomponent