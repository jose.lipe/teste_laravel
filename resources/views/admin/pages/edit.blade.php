@component('admin.layouts.elements.body')
    @slot('title') Páginas @endslot
    @slot('description') Administração de Páginas @endslot

    <form class="form-horizontal" action="{{ route('pages.update', $page->id)  }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        @include('admin.pages.form')
    </form>

    <a href="{{ route('pages.show', $page->id) }}" class="btn btn-xs btn-default">voltar</a>
@endcomponent